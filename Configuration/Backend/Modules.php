<?php

return [
    'drk_help' => [
        'parent' => 'help',
        'position' => ['before' => 'about'],
        'access' => 'user',
        'workspaces' => '*',
        'path' => '/module/help/drk_help',
        'labels' => 'LLL:EXT:examples/Resources/Private/Language/Module/locallang_mod.xlf',
        'extensionName' => 'drk_help',
        'controllerActions' => [
            \Fourviewture\DrkHelp\Controller\HelpController::class => [
                'index',
            ],
        ],
        'icon' => 'EXT:drk_help/Resources/Public/Images/logo-drk.svg',
        'labels' => 'LLL:EXT:drk_help/Resources/Private/Language/locallang_mod.xlf',
    ],
];
