window.addEventListener(
  "load",
  (event) => {
    var elements = document.getElementsByClassName("btn");

    var copyToClipBoard = function() {
      var attribute = this.getAttribute("data-clipboard-target");
      if (attribute != null) {
        var text = document.querySelector(attribute).value;
        navigator.clipboard.writeText(text);
      }
    };

    for (var i = 0; i < elements.length; i++) {
      elements[i].addEventListener('click', copyToClipBoard, false);
    }
  }
);
