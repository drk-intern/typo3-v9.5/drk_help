<?php
// Patched by Stefan Bublies@dt-internet.de, Auftrag von Herrn Ungeheuer am 06.03.2017 für die neuen Inhalte
namespace Fourviewture\DrkHelp\Controller;

use Fourviewture\DrkHelp\Utility\CheckPackagesUtility;
use Fourviewture\DrkHelp\Utility\CheckSysTemplateUtility;
use Fourviewture\DrkHelp\Utility\FeatureUtility;
use TYPO3\CMS\Backend\Template\ModuleTemplateFactory;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Information\Typo3Version;
use TYPO3\CMS\Core\Log\Logger;
use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Core\Log\Writer\FileWriter;
use TYPO3\CMS\Core\Package\PackageInterface;
use TYPO3\CMS\Backend\Attribute\Controller;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

#[Controller]
class HelpController extends ActionController
{
    /**
     *
     * @var CheckPackagesUtility
     */
    public $checkPackagesUtility;

    /**
     *
     * @var CheckSysTemplateUtility
     */
    public $checkSysTemplate;

    public function __construct(
        protected readonly ModuleTemplateFactory $moduleTemplateFactory,
    ) {
    }

    public function injectCheckPackagesUtility(CheckPackagesUtility $checkPackagesUtility)
    {
        $this->checkPackagesUtility = $checkPackagesUtility;
    }

    public function injectCheckSysTemplateUtility(CheckSysTemplateUtility $checkSysTemplate)
    {
        $this->checkSysTemplate = $checkSysTemplate;
    }


    public function indexAction()
    {
        $version = new Typo3Version();
        $this->view->assign(
            'versionTYPO3',
            $version->getVersion()
        );
        $this->view->assign(
            'versionTemplate',
            $this->checkPackagesUtility->getPackageMetadata('drk_template')
        );
        $this->view->assign(
            'features',
            FeatureUtility::getFeaturesAndStateWithPrefix('drk')
        );

        $packages = $this->checkPackagesUtility->getPackages();

        $this->view->assign(
            'supportData',
            [
                'system' => [
                    'HTTP_HOST' => $_SERVER['HTTP_HOST'],
                    'SERVER_ADDR' => $_SERVER['SERVER_ADDR'],
                    'DOCUMENT_ROOT' => $_SERVER['DOCUMENT_ROOT'],
                ],
                'packages' => $packages,
                'additionalPackages' => $this->checkPackagesUtility->getAddtionalExtensions($packages),
                'tables' => [
                    'sys_template' => $this->checkSysTemplate->getProblematicTemplates()
                ]
            ]
        );

        $this->view->assign(
            'logfiles',
            $this->getLogfileData()
        );

        $this->view->assign(
            'links',
            [
                [
                    'title' => 'Handbücher',
                    'links' => [
                        [
                            'title' => 'Online Handbuch',
                            'subtitle' => 'DRK Service GmbH',
                            'icon' => 'EXT:drk_help/Resources/Public/Images/logo-drk.svg',
                            'uri' => 'https://web-handbuch.drk-intern.de',
                            'description' => 'Handbuch zur DRK Musterstadt, Anleitungen, Historie und alle Informationen rund um die Musterseiten.'
                        ], [
                            'title' => 'DRK Musterseiten',
                            'icon' => 'EXT:drk_help/Resources/Public/Images/logo-drk.svg',
                            'subtitle' => 'DRK Service GmbH',
                            'uri' => 'https://www.drk-intern.de/musterseiten/musterseiten.html',
                            'description' => 'Offizielle öffentliche Webseite rund um die Musterwebseiten'
                        ], [
                            'title' => 'Problemlösungen',
                            'icon' => 'EXT:drk_help/Resources/Public/Images/logo-drk.svg',
                            'subtitle' => 'DRK Service GmbH',
                            'uri' => 'https://www.drk-intern.de/musterseiten/musterseiten.html',
                            'description' => 'Problemlösungen der DRK Service GmbH.'
                        ], [
                            'title' => 'Hilfebereich von D&T',
                            'icon' => 'EXT:drk_help/Resources/Public/Images/dt-kloetze.svg',
                            'subtitle' => 'D&T Internet GmbH',
                            'uri' => 'https://www.dt-internet.de/hilfe/drkcms-hilfe/',
                            'description' => 'DRKCMS-Hilfebereich von D&T Internet, hier finden Sie die aktuellsten Infos zu Ihrem DRKCMS bei D&T Internet.'
                        ],
                    ]
                ], [
                    'title' => 'Community',
                    'links' => [
                        [
                            'title' => 'Gitlab mit dem Musterseiten',
                            'icon' => 'EXT:drk_help/Resources/Public/Images/logo-drk.svg',
                            'subtitle' => 'DRK Service GmbH',
                            'uri' => 'https://gitlab.com/drk-intern/typo3-main-distribution',
                            'description' => 'Hier findest du die aktuellsten Versionen und Hinweise. Außerdem kannst du Fehler melden und so die Distribution für alle verbessern.'
                        ], [
                            'title' => 'TYPO3 Slack Channel',
                            'icon' => 'EXT:backend/Resources/Public/Images/typo3_logo_orange.svg',
                            'subtitle' => 'TYPO3 Community',
                            'uri' => 'https://typo3.slack.com',
                            'description' => 'Offizieller Channel der TYPO3 Community, hier gibt es einen Channel #cig-drk in dem Interessierte sich austauschen.'
                        ], [
                            'title' => 'TYPO3 Dokumentation',
                            'icon' => 'EXT:backend/Resources/Public/Images/typo3_logo_orange.svg',
                            'subtitle' => 'TYPO3 Community',
                            'uri' => 'https://docs.typo3.org',
                            'description' => 'Offizielle Dokumentation der TYPO3 Community, die Dokumentation liegt in englisch vor.'
                        ],
                    ]
                ], [
                    'title' => 'Styleguides',
                    'links' => [
                        [
                            'title' => 'DRK Styleguide',
                            'icon' => 'EXT:drk_help/Resources/Public/Images/logo-drk.svg',
                            'subtitle' => 'DRK Service GmbH',
                            'uri' => 'https://styleguide.drk.de/deutsches-rotes-kreuz/online-anwendungen/webseite',
                            'description' => ' Die Gestaltung der DRK-Webseite ist entsprechend des DRK-Erscheinungsbildes zurückhaltend und klar strukturiert. Im Zentrum einer jeden Seite steht das Bild als „Bühnenbild“; die Auswahl der Inhalte folgt dem Prinzip „weniger ist mehr“.'
                        ], [
                            'title' => 'Wasserwacht Styleguide',
                            'icon' => 'EXT:drk_help/Resources/Public/Images/logo-drk.svg',
                            'subtitle' => 'TYPO3 Community',
                            'uri' => 'https://styleguide.drk.de/wasserwacht/online-anwendungen/wasserwacht-webseite',
                            'description' => 'Die Gestaltung der Webseite ist entsprechend des Erscheinungsbildes zurückhaltend und klar strukturiert. Im Zentrum einer jeden Seite steht das Bild als „Bühnenbild“; die Auswahl der Inhalte folgt dem Prinzip „weniger ist mehr“. '
                        ], [
                            'title' => 'JRK Styleguide',
                            'icon' => 'EXT:drk_help/Resources/Public/Images/logo-drk.svg',
                            'subtitle' => 'TYPO3 Community',
                            'uri' => 'https://styleguide.drk.de/jugendrotkreuz',
                            'description' => 'Das Jugendrotkreuz verfügt über einen eigenen Online-Styleguide, in dem die wichtigsten Gestaltungsmerkmale und Anwendungen definiert sind.'
                        ],
                    ]
                ]
            ]
        );

        $moduleTemplate = $this->moduleTemplateFactory->create($this->request);
        // Adding title, menus, buttons, etc. using $moduleTemplate ...
        $moduleTemplate->setContent($this->view->render());
        return $this->htmlResponse($moduleTemplate->renderContent());
    }

    protected function getLogfileData(): array
    {
        $logManager = GeneralUtility::makeInstance(LogManager::class);
        $loggers = $logManager->getLoggerNames();
        $data = [
            'writers' => [],
            'logfiles' => []
        ];
        foreach ($loggers as $loggerName) {
            $logger = $logManager->getLogger($loggerName);
            if ($logger instanceof Logger) {
                $writers = $logger->getWriters();
                foreach ($writers as $writer) {
                    if ($writer[0] instanceof FileWriter) {
                        $data['writers'][$loggerName][get_class($writer[0])] = [
                            'file' => $writer[0]->getLogFile()
                        ];
                        $data['logfiles'][$writer[0]->getLogFile()] = '';
                        continue;
                    }
                    $data['writers'][$loggerName][get_class($writer[0])] = 'null';
                }
            }
        }

        foreach ($data['logfiles'] as $file => $content) {
            if (!is_string($file)) {
                continue;
            }
            $data['logfiles'][$file] = array_slice(
                $this->getFileContent($file, 20),
                -20,
                20,
                true
            );
        }

        return $data;
    }

    protected function getFileContent(string $path, int $linesCount = 10): array
    {
        if (!file_exists($path)) {
            return [
                'No logfile found at the expected location. Ask your admin for more information.'
            ];
        }
        $file = new \SplFileObject($path, 'r');
        $file->seek(PHP_INT_MAX);
        $last_line = $file->key();
        $offset = $last_line - $linesCount;
        if ($offset <= 0) {
            $offset = 0;
        }
        if (($offset === 0) && ($last_line === 0)) {
            return [];
        }
        $lines = new \LimitIterator($file, $offset, $last_line);
        return iterator_to_array($lines);
    }
}
