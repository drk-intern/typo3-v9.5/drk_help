<?php

namespace Fourviewture\DrkHelp\Utility;

use TYPO3\CMS\Core\Configuration\ConfigurationManager;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class FeatureUtility
{
    static public function getFeaturesAndStateWithPrefix($prefix)
    {
        /** @var ConfigurationManager $configurationManager */
        // normally would use this, but somehow the list of values is filtered
        // $configurationManager = GeneralUtility::makeInstance(ConfigurationManager::class);
        // $configuredFeatures = $configurationManager->getConfigurationValueByPath('SYS/features');
        $configuredFeatures = $GLOBALS['TYPO3_CONF_VARS']['SYS']['features'];

        if ($prefix === '') {
            return $configuredFeatures;
        }

        $prefixedFeatures = [];
        foreach($configuredFeatures as $featureName => $featureState) {
            if (strpos($featureName, $prefix) === 0) {
                $prefixedFeatures[$featureName] = $featureState;
            }
        }
        return $prefixedFeatures;
    }
}
