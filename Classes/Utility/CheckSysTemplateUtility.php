<?php

namespace Fourviewture\DrkHelp\Utility;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Package\PackageInterface;
use TYPO3\CMS\Core\Package\PackageManager;

class CheckSysTemplateUtility
{

    public function __construct(
        private readonly ConnectionPool $connectionPool,
    ) {}

    public function getProblematicTemplates(): array
    {
        $connection = $this->connectionPool->getConnectionForTable('sys_template');
        $rows = $connection->select(
            ['uid', 'pid', 'config'],
            'sys_template',
            [
                'deleted' => 0,
                'hidden' => 0
            ]
        )->fetchAllAssociative();
        $result = [];
        foreach ($rows as $row) {
            if (trim($row['config'] ?? '') !== '') {
                $result[] = [
                    'row' => $row,
                    'error' => 'Dieses Templates enthält eigenes TypoScript, das kann Probleme bei Updates verursachen.',
                    'solutions' => [
                        [
                            'summary' => 'Durch die Verwendung von eigenem TS kann das DRKCMS angepasst werden, dies kann aber zu Problemen bei Updates führen. Empfehlung, entfernen Sie das eigene TypoScript',
                            'url' => ''
                        ]
                    ]
                ];
            }
            if (strpos($row['config'] ?? '', 'baseURL') !== false) {
                $result[] = [
                    'row' => $row,
                    'error' => 'Verwendet baseURL, damit kann es zu Darstellungsproblemen kommen.',
                    'solutions' => [
                        [
                            'summary' => 'config.baseURL sollte nicht verwendet werden. Verwendet die SiteConfig, normalerweise kann der Eintrag einfach entfernt werden.',
                            'url' => 'https://docs.typo3.org/c/typo3/cms-core/main/en-us/Changelog/12.1/Deprecation-99170-ConfigbaseURLAndBaseTagFunctionality.html'
                        ]
                    ]
                ];
            }
        }
        return $result;
    }
}
