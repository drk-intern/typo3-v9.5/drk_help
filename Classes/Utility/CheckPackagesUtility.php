<?php

namespace Fourviewture\DrkHelp\Utility;

use TYPO3\CMS\Core\Package\MetaData;
use TYPO3\CMS\Core\Package\PackageInterface;
use TYPO3\CMS\Core\Package\PackageManager;

class CheckPackagesUtility
{
    /**
     *
     * @var PackageManager
     */
    public $packageManager;

    protected $expectedPackages = [
        '4viewture/twoclick',
        'b13/container',
        'b13/menus',
        'bacon/bacon-qr-code',
        'causal/image_autoresize',
        'christian-riesen/base32',
        'contentblocks/content-blocks',
        'composer/ca-bundle',
        'composer/class-map-generator',
        'composer/composer',
        'composer/metadata-minifier',
        'composer/pcre',
        'composer/semver',
        'composer/spdx-licenses',
        'composer/xdebug-handler',
        'dasprid/enum',
        'doctrine/annotations',
        'doctrine/cache',
        'doctrine/dbal',
        'doctrine/deprecations',
        'doctrine/event-manager',
        'doctrine/instantiator',
        'doctrine/lexer',
        'drkservice/blog',
        'drkservice/publications',
        'drkservice/drkservice-app-buttons',
        'drk-service/drk-typo3-updater',
        'drk/calltoaction',
        'drk/drk-addresses',
        'drk/drk-clothescontainersearch',
        'drk/drk-clothescontainerview',
        'drk/drk-contactform',
        'drk/drk-courseregistration',
        'drk/drk-coursesearch',
        'drk/drk-courseview',
        'drk/drk-distribution',
        'drk/drk-donate',
        'drk/drk-general',
        'drk/drk-help',
        'drk/drk-honoraryform',
        'drk/drk-jobboard',
        'drk/drk-memberform',
        'drk/drk-supply-finder',
        'drk/fal-webdav',
        'drk/linklist',
        'drk/template',
        'drkservice/blog',
        'drkservice/publications',
        'drkservice/drk-orderform',
        'drkservice/drkservice-distribution-kv',
        'drkservice/drkservice-iframe',
        'egulias/email-validator',
        'enshrined/svg-sanitize',
        'erecht24/er24-rechtstexte',
        'ezyang/htmlpurifier',
        'firebase/php-jwt',
        'fluidtypo3/vhs',
        'georgringer/news',
        'georgringer/news-administration',
        'georgringer/numbered-pagination',
        'guzzlehttp/guzzle',
        'guzzlehttp/promises',
        'guzzlehttp/psr7',
        'helhum/config-loader',
        'helhum/php-error-reporting',
        'helhum/typo3-console',
        'ichhabrecht/content-defender',
        'justinrainbow/json-schema',
        'kaystrobach/newssync',
        'lolli42/finediff',
        'masterminds/html5',
        'mpdf/mpdf',
        'mpdf/psr-http-message-shim',
        'mpdf/psr-log-aware-trait',
        'myclabs/deep-copy',
        'nikic/php-parser',
        'paragonie/random_compat',
        'phpdocumentor/reflection-common',
        'phpdocumentor/reflection-docblock',
        'phpdocumentor/type-resolver',
        'phpstan/phpdoc-parser',
        'pluswerk/plus-drk-contentelements',
        'psr/cache',
        'psr/clock',
        'psr/container',
        'psr/event-dispatcher',
        'psr/http-client',
        'psr/http-factory',
        'psr/http-message',
        'psr/http-server-handler',
        'psr/http-server-middleware',
        'psr/log',
        'raccoondepot/error-log',
        'ralouphie/getallheaders',
        'react/promise',
        'sabre/dav',
        'sabre/event',
        'sabre/http',
        'sabre/uri',
        'sabre/vobject',
        'sabre/xml',
        'seld/jsonlint',
        'seld/phar-utils',
        'seld/signal-handler',
        'setasign/fpdi',
        'symfony/cache',
        'symfony/cache-contracts',
        'symfony/clock',
        'symfony/config',
        'symfony/console',
        'symfony/dependency-injection',
        'symfony/deprecation-contracts',
        'symfony/doctrine-messenger',
        'symfony/event-dispatcher',
        'symfony/event-dispatcher-contracts',
        'symfony/expression-language',
        'symfony/filesystem',
        'symfony/finder',
        'symfony/http-foundation',
        'symfony/mailer',
        'symfony/messenger',
        'symfony/mime',
        'symfony/options-resolver',
        'symfony/polyfill-ctype',
        'symfony/polyfill-intl-grapheme',
        'symfony/polyfill-intl-idn',
        'symfony/polyfill-intl-normalizer',
        'symfony/polyfill-mbstring',
        'symfony/polyfill-php72',
        'symfony/polyfill-php73',
        'symfony/polyfill-php80',
        'symfony/polyfill-php81',
        'symfony/polyfill-php83',
        'symfony/polyfill-uuid',
        'symfony/process',
        'symfony/property-access',
        'symfony/property-info',
        'symfony/rate-limiter',
        'symfony/routing',
        'symfony/service-contracts',
        'symfony/string',
        'symfony/type-info',
        'symfony/uid',
        'symfony/var-dumper',
        'symfony/var-exporter',
        'symfony/yaml',
        'tpwd/ke_search',
        'typo3/class-alias-loader',
        'typo3/cms-adminpanel',
        'typo3/cms-backend',
        'typo3/cms-belog',
        'typo3/cms-beuser',
        'typo3/cms-cli',
        'typo3/cms-composer-installers',
        'typo3/cms-core',
        'typo3/cms-dashboard',
        'typo3/cms-extbase',
        'typo3/cms-extensionmanager',
        'typo3/cms-felogin',
        'typo3/cms-filelist',
        'typo3/cms-filemetadata',
        'typo3/cms-fluid',
        'typo3/cms-fluid-styled-content',
        'typo3/cms-form',
        'typo3/cms-frontend',
        'typo3/cms-impexp',
        'typo3/cms-indexed-search',
        'typo3/cms-info',
        'typo3/cms-install',
        'typo3/cms-linkvalidator',
        'typo3/cms-lowlevel',
        'typo3/cms-opendocs',
        'typo3/cms-reactions',
        'typo3/cms-recycler',
        'typo3/cms-redirects',
        'typo3/cms-reports',
        'typo3/cms-rte-ckeditor',
        'typo3/cms-scheduler',
        'typo3/cms-seo',
        'typo3/cms-setup',
        'typo3/cms-sys-note',
        'typo3/cms-t3editor',
        'typo3/cms-tstemplate',
        'typo3/cms-viewpage',
        'typo3/cms-webhooks',
        'typo3/html-sanitizer',
        'typo3fluid/fluid',
        'webmozart/assert',
        'yoast-seo-for-typo3/yoast_seo',
        'zeroseven/z7-semantilizer',
    ];

    public function injectPackageManager(PackageManager $packageManager)
    {
        $this->packageManager = $packageManager;
    }

    public function getPackageMetadata(string $packageKey): MetaData
    {
        return $this->packageManager->getPackage('drk_template')->getPackageMetaData();
    }


    public function getPackages(): array
    {
        $packages = [];
        foreach ($this->packageManager->getActivePackages() as $package) {
            if ($package instanceof PackageInterface) {
                $packages[$package->getPackageKey()] = [
                    'packageKey' => $package->getPackageKey(),
                    'packagePath' => $package->getPackagePath(),
                    'packageReplacementKeys' => $package->getPackageReplacementKeys(),
                    'packageMetaData' => [
                        'version' => $package->getPackageMetaData()->getVersion(),
                        'packageType' => $package->getPackageMetaData()->getPackageType(),
                        'constraints' => $package->getPackageMetaData()->getConstraints(),
                        'isExtensionType' => $package->getPackageMetaData()->isExtensionType(),
                        'isFrameworkType' => $package->getPackageMetaData()->isFrameworkType()
                    ],
                    'valueFromComposerManifest' => $package->getValueFromComposerManifest()
                ];
            }
        }
        return $packages;
    }

    public function getAddtionalExtensions(array $installedPackages): array
    {
        $additionalPackages = [];
        foreach ($installedPackages as $key => $package) {
            if (!in_array($package['valueFromComposerManifest']->name, $this->expectedPackages)) {
                $additionalPackages[$key] = $package;
            }
        }
        return $additionalPackages;
    }

    protected function getTemplatesAndCheckResult(): array
    {
        return [];
    }
}
