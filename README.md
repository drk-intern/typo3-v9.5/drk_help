# Installation

Just install the extension via extensionmanager

# About

This extension fetches the content from:

* http://www.drk-intern.de/musterseiten/problemloesungen.html
* reduces content to `//*[@id="col3_content"]` (XPath)
* replaces `href="musterseiten/problemloesungen.html` by `` to fix relative links
* replaces `src="fileadmin` by `src="http://www.drk-intern.de/fileadmin` to fix images