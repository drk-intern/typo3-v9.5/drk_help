<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: 'contentmigration'
 *
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * 'version' and 'dependencies' must not be touched!
 ***************************************************************/

$EM_CONF['drk_help'] = array(
    'title' => 'Help Module',
    'description' => 'Allows to integrate help from drk-intern.de',
    'category' => 'templates',
    'author' => '4viewture GmbH - Kay Strobach',
    'author_email' => 'drk-service@support.4viewture.de',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '12.10.0',
    'constraints' => array(
        'depends' => array(
            'typo3' => '10.4.0-10.4.99',
        ),
        'conflicts' => array(),
        'suggests' => array(),
    )
);
